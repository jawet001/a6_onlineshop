package de.fhswf.fit;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity
public class Image {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column
    private String dir;
    
    @Column
    private ImageType type;

    public Image() {
        name = "";
        dir = null;
        type = ImageType.JPEG;
    }

    public Image(String name, String dir, ImageType type) {
        this.name = name;
        this.dir = dir;
        this.type = type;
    }

    public String getDir() {
        return this.dir;
    }
}