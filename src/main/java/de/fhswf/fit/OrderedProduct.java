package de.fhswf.fit;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;

@Entity
public class OrderedProduct {
    
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @JoinColumn
    @OneToOne(cascade = CascadeType.PERSIST)
    private Product product;

    @JoinColumn
    @OneToOne(cascade = CascadeType.PERSIST)
    private Orders order;

    @Column
    private int amount;

}
