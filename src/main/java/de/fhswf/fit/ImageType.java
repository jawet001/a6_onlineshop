package de.fhswf.fit;

enum ImageType {
    JPEG,
    PNG,
    GIF
}