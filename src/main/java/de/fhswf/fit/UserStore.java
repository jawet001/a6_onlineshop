package de.fhswf.fit;

import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

@PersistenceUnit
@Stateless
public class UserStore {

    @PersistenceContext
    EntityManager em;

    public List<Users> getAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> cq = cb.createQuery(Users.class);
        Root<Users> root = cq.from(Users.class);
        cq.select(root);
        TypedQuery<Users> query = em.createQuery(cq);
        return query.getResultList();
    }

    public List<Users> findByFilter(Integer id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> c = q.from(Users.class);

        System.out.println(String.format("id: %s", id));

        if (id != null) {
            q.where(cb.equal(c.get("id"), id));
        }

        TypedQuery<Users> query = em.createQuery(q);

        List<Users> users = query.getResultList();

        for (Users u : users) {
            System.out.println("Benutzer: " + u.getSubject());
        }

        return users;
    }

    public Users findBySubject(String subject) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> q = cb.createQuery(Users.class);
        Root<Users> c = q.from(Users.class);

        System.out.println(String.format("subject: %s", subject));

        if (subject != null && !subject.equals("")) {
            q.where(cb.equal(c.get("subject"), subject));
        }

        TypedQuery<Users> query = em.createQuery(q);

        Users user = null;
        try {
            user = query.getSingleResult();
            System.out.println("Benutzer: " + user.getSubject());
        } catch (NoResultException ex) {
            System.out.println("Keinen Benutzer gefunden.");
        }

        return user;
    }

    public Users add(Users o) {
        em.persist(o);
        return o;
    }

    public Users update(Users o) {
        em.merge(o);
        return o;
    }

    public void delete(Users o) {
        o = em.merge(o);
        em.remove(o);
    }

    public void delete(Integer id) {
        Users o = findByFilter(id).get(0);
        if (o != null) {
            em.remove(o);
        } else {
            System.out.println("ID nicht gefunden. " + id);
        } 
    }
}