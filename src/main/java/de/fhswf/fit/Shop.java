package de.fhswf.fit;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.annotation.ManagedProperty;

@Named("shop")
@SessionScoped
public class Shop implements Serializable {
    
    transient ShopStore shopStore;

    String filterID = "";
    String filterName = "";
    
    @Inject
    @ManagedProperty(value="#{nickname}")
    String nickname;

    List<Product> products = new ArrayList<Product>();

    public void init() {

        System.out.println("initializing Shop");

        products = this.shopStore.getAll();

        if (products.size() == 0) {
            System.out.println("Adding Product.");

            ArrayList<Image> testImages = new ArrayList<Image>();
            testImages.add(new Image("Testbild 1", "./assets/testbild_1.jpg", ImageType.JPEG));

            ArrayList<Category> testCategories = new ArrayList<Category>();
            testCategories.add(new Category("Testkategorie", "Kategorie für Testzwecke"));

            addProduct(
                new Product(
                    "ABCDEFGH", 
                    "Test", 
                    new BigDecimal(1.5), 
                    testImages, 
                    testCategories, 
                    10, 
                    "Beschreibende Beschreibung"
                )
            );

            products = this.shopStore.getAll();
        } else {
            System.out.println("Anzahl Produkte: " + products.size());
        }

        this.products.addAll(products);

    }

    @Inject
    public void setShopStore(ShopStore shopStore) {
        this.shopStore = shopStore;
        init();
    }

    /*private List<Product> products = new ArrayList<Product>();
    public List<Product> listProducts() {
        return this.shopStore.getAll();
    }*/

    public void addProduct(Product product) {
        shopStore.add(product);
        products = shopStore.getAll();
    }

    public void deleteProduct(String id) {
        shopStore.delete(id);
    }

    public String getFilterID() {
        return this.filterID;
    }

    public void setFilterID(String filterID) {
        this.filterID = filterID;
    }

    public String getFilterName() {
        return this.filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public List<Product> getProducts() {
        return this.products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    
    public void filterProducts() {

        products.clear();
        products.addAll(this.shopStore.findByFilter(filterID, filterName));

    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
