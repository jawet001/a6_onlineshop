package de.fhswf.fit;

import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

@PersistenceUnit
@Stateless
public class ShopStore {

    @PersistenceContext
    EntityManager em;

    public List<Product> getAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(Product.class);
        Root<Product> root = cq.from(Product.class);
        cq.select(root);
        TypedQuery<Product> query = em.createQuery(cq);
        return query.getResultList();
    }

    public List<Product> findByFilter(String id, String name) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> q = cb.createQuery(Product.class);
        Root<Product> c = q.from(Product.class);

        System.out.println(String.format("productNumber: %s, name: %s", id, name));

        if (id != null && !id.isEmpty()) {
            q.where(cb.equal(c.get("productNumber"), id));
        }

        if (name != null && !name.isEmpty()) {
            q.where(cb.equal(c.get("name"), name));
        }

        TypedQuery<Product> query = em.createQuery(q);

        List<Product> products = query.getResultList();

        for (Product p : products) {
            System.out.println(p.getProductNumber() + ": " + p.getName());
        }

        return products;
    }

    public Product add(Product o) {
        em.persist(o);
        return o;
    }

    public Product update(Product o) {
        em.merge(o);
        return o;
    }

    public void delete(Product o) {
        o = em.merge(o);
        em.remove(o);
    }

    public void delete(String id) {
        Product o = findByFilter(id, "").get(0);
        if (o != null) {
            em.remove(o);
        } else {
            System.out.println("ID nicht gefunden. " + id);
        } 
    }
}