package de.fhswf.fit;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.annotation.ManagedProperty;

@Named("user")
@SessionScoped
public class bUser implements Serializable {
    
    transient UserStore userStore;

    Integer filterID = 0;
    String filterSubject = "";
    
    @Inject
    @ManagedProperty(value="#{nickname}")
    String nickname;



    List<Users> users = new ArrayList<Users>();

    public void init() {

        System.out.println("initializing Users");

        users = this.userStore.getAll();

        if (users.size() == 0) {
            System.out.println("Adding User.");

            ArrayList<Image> testImages = new ArrayList<Image>();
            testImages.add(new Image("Testbild 1", "./assets/testbild_1.jpg", ImageType.JPEG));

            ArrayList<Category> testCategories = new ArrayList<Category>();
            testCategories.add(new Category("Testkategorie", "Kategorie für Testzwecke"));

            ArrayList<OrderedProduct> orderedProducts = new ArrayList<OrderedProduct>();
            OrderedProduct op = new OrderedProduct();
            orderedProducts.add(op);
            ArrayList<Orders> orders = new ArrayList<Orders>();
            ArrayList<Address> addresses = new ArrayList<Address>();
            Address a = new Address();
            addresses.add(a);
            String orderComments = "";
            Orders order = new Orders("0", addresses.get(0), addresses.get(0), orderedProducts, orderComments);
            orders.add(order);

            addUser(
                new Users(
                    0, 
                    nickname, 
                    addresses, 
                    orders)
            );

            users = this.userStore.getAll();
        } else {
            System.out.println("Anzahl Benutzer: " + users.size());
        }

        this.users.addAll(users);

    }

    @Inject
    public void setUserStore(UserStore userStore) {
        this.userStore = userStore;
        init();
    }

    /*private List<Product> products = new ArrayList<Product>();
    public List<Product> listProducts() {
        return this.shopStore.getAll();
    }*/

    public void addUser(Users user) {
        userStore.add(user);
        users = userStore.getAll();
    }

    public List<Orders> getOrders() {
        return users.get(0).getOrders();
    }

    public void deleteUser(Integer id) {
        userStore.delete(id);
    }

    public Integer getFilterID() {
        return this.filterID;
    }

    public void setFilterID(Integer filterID) {
        this.filterID = filterID;
    }

    public String getFilterSubject() {
        return this.filterSubject;
    }

    public void setFilterSubject(String filterSubject) {
        this.filterSubject = filterSubject;
    }

    public List<Users> getUsers() {
        return this.users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }
    
    public void filterUsers() {
        users.clear();
        users.add(this.userStore.findBySubject(filterSubject));
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getID() {
        return users.get(0).getID();
    }
}
