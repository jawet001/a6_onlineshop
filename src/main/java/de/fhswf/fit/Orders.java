package de.fhswf.fit;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;

@Entity
public class Orders {
    
    @Id
    @Column
    private String orderNumber;

    @JoinColumn
    private Address billingAddress;

    @JoinColumn
    private Address deliveryAddress;

    @JoinColumn
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<OrderedProduct> orderedProducts;

    @Column
    private String orderComments;

    public Orders() {
        orderNumber = "";
        billingAddress = new Address();
        deliveryAddress = new Address();
        orderedProducts = new ArrayList<OrderedProduct>();
        orderComments = "";
    }

    public Orders(String orderNumber, Address billingAddress, Address deliveryAddress, List<OrderedProduct> orderedProducts, String orderComments) {
        this.orderNumber = orderNumber;
        this.billingAddress = billingAddress;
        this.deliveryAddress = deliveryAddress;
        this.orderedProducts = orderedProducts;
        this.orderComments = orderComments;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber() {
        return this.orderNumber;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Address getBillingAddress() {
        return this.billingAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Address getDeliveryAddress() {
        return this.deliveryAddress;
    }

}
