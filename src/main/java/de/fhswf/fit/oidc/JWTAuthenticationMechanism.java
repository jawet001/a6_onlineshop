package de.fhswf.fit.oidc;

import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import com.auth0.client.auth.AuthAPI;
import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.auth.TokenHolder;
import com.auth0.net.AuthRequest;

import de.fhswf.fit.UserStore;
import de.fhswf.fit.Users;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SigningKeyResolverAdapter;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.security.enterprise.AuthenticationStatus;
import jakarta.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import jakarta.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * This class implements authentication via a ID Token (JWT) returned by Auth0.
 * The class
 * intercepts a call to the {@code /callback} endpoint,
 * gets and verifies the ID Token,
 * and sets the {@link Principal} of the {@link HttpMessageContext} accordingly.
 * Additionally, the {@code Claims} attributes are written to session variables.
 *
 * @author Christian Gawron
 */
@ApplicationScoped
public class JWTAuthenticationMechanism implements HttpAuthenticationMechanism {

    static final Logger logger = Logger.getLogger(JWTAuthenticationMechanism.class.getName());

    transient UserStore userStore;
    
    @Inject
    public void setUserStore(UserStore userStore) {
        this.userStore = userStore;
    }

    /**
     * @implNote Don't publish a client secret of a productive application!
     */
    AuthAPI auth;

    Claims claims = null;

    static class KeyResolver extends SigningKeyResolverAdapter {
        private static String BASE64_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8Thg156woJzdg7HP/rKtaIOcIntoqoP3BW0tQnb8sMV2e3hLSys2BW80o6KnqZjd1UNZenKW/u9j1Yq9oHxE34/6u5KmBO5AAXn1M+QkLa2Pp8TvxL+8IdaiNpzS5ZSmHkrFmN/gijRPOKsYWbEtPfsGCO5sMjj1VbbyHGVwT7TKJ7kVHbHFH1jAVB6nl7ibLUM0169UjWWTnI8zaWZNpZNToTGpIMGclDHowuO/eO81epfAj2OE7zKR98zjYiLRsEkJRB6fSSsMu8Dt+Ph0xsCB/klQviYJ77ztAlRkbm7su2m5loOqWXhNw+IFOG+LPrkZncX3dsyxtluuvuvnDwIDAQAB";

        @Override
        public Key resolveSigningKey(JwsHeader header, Claims claims) {
            JWTAuthenticationMechanism.logger.info("resolveSigningKey for claims: " + claims);
            try {
                byte[] encoded = Base64.getDecoder().decode(BASE64_KEY);
                X509EncodedKeySpec spec = new X509EncodedKeySpec(encoded);
                KeyFactory kf;

                kf = KeyFactory.getInstance("RSA");
                Key key = kf.generatePublic(spec);
                JWTAuthenticationMechanism.logger.info("resolveSigningKey: " + key);
                return key;
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                JWTAuthenticationMechanism.logger.severe("Error while resolving signing key: " + e.getMessage());
                throw new RuntimeException(e);
            }

        }
    }

    public static class MyPrincipal implements Principal {
        private String name;

        public MyPrincipal(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }
    }

    public JWTAuthenticationMechanism() {

        auth = new AuthAPI("cgawron.eu.auth0.com", "BEKLPhg2FPML0prWWnSiYieOd1OH6Ma0",
                "y9JICmALZi6lgAXrUh9GysWiJsbWOGo5s0OPU880--60-94-sLkrZoGsKJX6M996");
    }

    @Override
    public AuthenticationStatus validateRequest(HttpServletRequest req,
            HttpServletResponse res,
            HttpMessageContext msg) {
        logger.info("JWTAuthenticationMechanism: validateRequest");
        String code = req.getParameter("code");
        String error = req.getParameter("error");
        String baseUri = req.getScheme() + "://" + req.getServerName();
        if ((req.getScheme().equals("http") && req.getServerPort() != 80)
                || (req.getScheme().equals("https") && req.getServerPort() != 443)) {
            baseUri += ":" + req.getServerPort();
        }

        if (req.getRequestURI().equals("/callback")) {

            if (error != null) {
                return msg.responseUnauthorized();
            } else {

                String redirectUri = baseUri + "/callback";
                AuthRequest request = auth.exchangeCode(code, redirectUri)
                        .setScope("openid profile email");
                try {
                    TokenHolder holder = request.execute();
                    logger.info("IDToken: " + holder.getIdToken());
                    logger.info("AccessToken: " + holder.getAccessToken());
                    Jws<Claims> jws = Jwts.parser()
                            .setSigningKeyResolver(new KeyResolver())
                            .parseClaimsJws(holder.getIdToken());
                    logger.info("JWT: " + jws.getBody());
                    claims = jws.getBody();
                    claims.keySet().forEach(key -> {
                        logger.info(key + ": " + claims.get(key));
                        req.getSession().setAttribute(key, claims.get(key));
                    });
                    logger.info("Session Inactive Interval: " + req.getSession().getMaxInactiveInterval());
                    Principal principal = new MyPrincipal(claims.getSubject());
                    Set<String> groups = new HashSet<>();
                    groups.add("user");
                    msg.notifyContainerAboutLogin(principal, groups);

                    Users u = userStore.findBySubject((String)claims.get("email"));
                    if (u == null) {
                        u = new Users();
                        u.setSubject((String)claims.get("email"));
                        u = userStore.add(u);
                    }
                    logger.info("User ID: " + u.getID());

                    return msg.redirect(baseUri);
                } catch (APIException exception) {
                    logger.severe("Error while exchanging code: " + exception.getMessage());
                    return msg.responseUnauthorized();
                } catch (Auth0Exception exception) {
                    logger.severe("Error while exchanging code: " + exception.getMessage());
                    return msg.responseUnauthorized();
                }
            }
        } else if (req.getRequestURI().equals("/login")) {
            String redirectUri = baseUri + "/callback";
            String authorizeUrl = auth.authorizeUrl(redirectUri)
                    .withScope("openid profile email")
                    .withState("state123")
                    .build();
            msg.redirect(authorizeUrl);
        } else if (req.getRequestURI().equals("/logout")) {

            String logoutUrl = auth.logoutUrl(baseUri + "/shop.xhtml", true)
                    .build();
            msg.cleanClientSubject();
            claims.keySet().forEach(key -> {
                req.getSession().removeAttribute(key);
            });
            claims = null;
            msg.redirect(logoutUrl);
        }
        return msg.doNothing();
    }
}
