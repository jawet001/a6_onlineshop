package de.fhswf.fit;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;

@Entity
public class Users {
    
    

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String subject;

    @JoinColumn
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Address> addresses;

    @JoinColumn
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Orders> orders;

    public Users() {
        id = 0;
        subject = "";
        addresses = new ArrayList<Address>() {};
        orders = new ArrayList<Orders>();
    }

    public Users(int id, String subject, ArrayList<Address> addresses, ArrayList<Orders> orders) {
        this.id = id;
        this.subject = subject;
        this.addresses = addresses;
        this.orders = orders;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    public List<Orders> getOrders() {
        return orders;
    }

}
