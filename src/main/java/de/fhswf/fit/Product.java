package de.fhswf.fit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;

@Entity
public class Product {
    @Id
    @Column
    private String productNumber;

    @Column
    private String name;

    @Column
    private BigDecimal price;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinColumn
    private List<Image> images;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinColumn
    private List<Category> categories;
    
    @Column
    private Integer inStock;

    @Column
    private String description;

    public Product() {
        productNumber = "";
        name = "";
        price = new BigDecimal(0.0);
        images = new ArrayList<Image>();
        categories = new ArrayList<Category>();
        inStock = 0;
        description = "";
    }

    public Product(String productNumber, String name, BigDecimal price, List<Image> images, List<Category> categories, Integer inStock, String description) {
        this.productNumber = productNumber;
        this.name = name;
        this.price = price;
        this.images = images;
        this.categories = categories;
        this.inStock = inStock;
        this.description = description;
    }

    public String getProductNumber() {
        return this.productNumber;
    }

    public String getName() {
        return this.name;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public List<Image> getImages() {
        return this.images;
    }

    public List<Category> getCategories() {
        return this.categories;
    }

    public int getInStock() {
        return this.inStock;
    }

    public String getDescription() {
        return this.description;
    }
}